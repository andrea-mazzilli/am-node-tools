const { LOG, EXEC } = require('../index')

const init = async () => {
  LOG({ text: 'TEST EXEC', color: 'green', clear: true })

  const nodeVersion = await EXEC({ 
    command: 'node -v'
  })
  if ( nodeVersion ) {
    LOG({ text: `Your Node version is ${nodeVersion.replace('v','').split('\n')[0]}`, color: 'blue' })
  } else {
    LOG({ text: `Node is not installed`, color: 'red' })
  }

  const brewVersion = await EXEC({ 
    command: 'brew -v'
  })
  if ( brewVersion ) {
    LOG({ text: `Your Brew version is ${brewVersion.replace('Homebrew ', '').split('\n')[0]}`, color: 'blue' })
  } else {
    LOG({ text: `Brew is not installed`, color: 'red' })
  }

  const yarnVersion = await EXEC({ 
    command: 'yarn -v'
  })
  if ( yarnVersion ) {
    LOG({ text: `Your Yarn version is ${yarnVersion.split('\n')[0]}`, color: 'blue' })
  } else {
    LOG({ text: `Yarn is not installed`, color: 'red' })
  }

  const unknownVersion = await EXEC({ 
    command: 'unknown -v'
  })
  if ( unknownVersion ) {
    LOG({ text: `Your Unknown version is ${unknownVersion.split('\n')[0]}`, color: 'blue' })
  } else {
    LOG({ text: `Unknown is not installed`, color: 'red' })
  }
}

init()
