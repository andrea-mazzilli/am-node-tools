const { LOG } = require('../index')

const init = () => {
  LOG({ text: 'TEST LOG', color: 'green', clear: true })
  LOG({ text: 'no color' })
  LOG({ text: `Red`, color: 'red' })
  LOG({ text: `Blue`, color: 'blue' })
  LOG({ text: `Green`, color: 'green' })
  LOG({ text: `Yellow`, color: 'yellow' })
}

init()