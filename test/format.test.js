const { LOG, FORMAT, FS } = require('../index')
const { MS_IN_SECOND, MS_IN_MINUTE, MS_IN_HOUR, MS_IN_DAY } = require('../helpers/date')
const package = {
  "name": "am-node-tools",
  "version": "1.0.1"
}

const init = async () => {
  LOG({ text: 'TEST FORMAT', color: 'green', clear: true })
  LOG({ text: 'JSON' })
  LOG({ text: JSON.stringify(package), color: 'grey' })
  LOG({ text: FORMAT.JSON( package ), color: 'green' })

  LOG({ text: 'FRIENDLY_DATE' })
  LOG({ text: 'less than a minute ago', color: 'grey' })
  LOG({ text: FORMAT.FRIENDLY_DATE( new Date().getTime() - ( MS_IN_MINUTE - 1000 ) ), color: 'green' })

  LOG({ text: 'FRIENDLY_DATE' })
  LOG({ text: '5 minutes ago', color: 'grey' })
  LOG({ text: FORMAT.FRIENDLY_DATE( new Date().getTime() - ( MS_IN_MINUTE * 5 ) ), color: 'green' })

  LOG({ text: 'FRIENDLY_DATE' })
  LOG({ text: '2 hours ago', color: 'grey' })
  LOG({ text: FORMAT.FRIENDLY_DATE( new Date().getTime() - ( MS_IN_HOUR * 2 ) ), color: 'green' })

  LOG({ text: 'FRIENDLY_DATE' })
  LOG({ text: '5 days ago', color: 'grey' })
  LOG({ text: FORMAT.FRIENDLY_DATE( new Date().getTime() - ( MS_IN_DAY * 5 ) ), color: 'green' })
}

init()