const { LOG, STATE } = require('../index')

const init = async () => {
  LOG({ text: 'TEST STATE', color: 'green', clear: true })

  LOG({ text: 'current state' })
  LOG({ text: JSON.stringify( STATE.GET() ), color: 'grey' })

  LOG({ text: 'state set "date" to "2020-10-29"', color: 'yellow' })
  STATE.SET({ key: 'date', value: '2020-10-29' })

  LOG({ text: 'current state' })
  LOG({ text: JSON.stringify( STATE.GET() ), color: 'grey' })

  LOG({ text: 'state set "email" to "andrea@email.com"', color: 'yellow' })
  STATE.SET({ key: 'email', value: 'andrea@email.com' })

  LOG({ text: 'current state' })
  LOG({ text: JSON.stringify( STATE.GET() ), color: 'grey' })

  LOG({ text: 'state set "name" to "andrea"', color: 'yellow' })
  STATE.SET({ key: 'name', value: 'andrea' })

  LOG({ text: 'current state' })
  LOG({ text: JSON.stringify( STATE.GET() ), color: 'grey' })
  
}

init()