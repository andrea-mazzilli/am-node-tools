const { LOG, FS, CRYPTO, WAIT } = require('../index')
const srcJSON = { name: 'andrea', email: 'mazzilli.andrea@gmail.com' }
const destJSON = 'image.json'
const imagePath = './image.png'
const init = async () => {
  LOG({ text: 'TEST CRYPTO', color: 'green', clear: true })

  LOG({ text: 'JSON_TO_PNG', color: 'blue' })
  await CRYPTO.JSON_TO_PNG({ json: srcJSON, path: imagePath })

  LOG({ text: 'PNG_TO_JSON', color: 'blue' })
  const jsonFromPngData = await CRYPTO.PNG_TO_JSON({ path: imagePath })

  LOG({ text: 'SAVE NEW JSON', color: 'blue' })
  await FS.CREATE_FILE({ path: './', name: destJSON, data: jsonFromPngData })

  await WAIT({ ms: 3000 })
  LOG({ text: 'REMOVE DATA', color: 'blue' })
  await FS.DELETE_FILE({ path: imagePath })
  await FS.DELETE_FILE({ path: './' + destJSON })
}

init()
