const { LOG, FS } = require('../index')
const path = './'
const newFolderName = 'deleteme'
const newFileName = 'deleteme.json'
const init = async () => {
  LOG({ text: 'TEST FS', color: 'green', clear: true })
  const isWritable = await FS.IS_WRITABLE({ path })
  if ( isWritable.success ) {
    LOG({ text: `${path} is writable`, color: 'blue' })
  } else {
    LOG({ text: `${path} is NOT writable`, color: 'red' })
  }
  const folderCreated = await FS.CREATE_FOLDER({ path, name: newFolderName })
  if ( folderCreated.success ) {
    LOG({ text: `Folder ${newFolderName} in ${path} was created`, color: 'blue' })
  } else {
    LOG({ text: `Folder ${newFolderName} in ${path} was NOT created`, color: 'red' })
  }
  const folderDeleted = await FS.DELETE_FOLDER({ path: path + newFolderName })
  if ( folderDeleted.success ) {
    LOG({ text: `Folder ${newFolderName} in ${path} was deleted`, color: 'blue' })
  } else {
    LOG({ text: `Folder ${newFolderName} in ${path} was NOT deleted`, color: 'red' })
  }
  const fileCreated = await FS.CREATE_FILE({ path, name: newFileName, data: 'text here' })
  if ( fileCreated.success ) {
    LOG({ text: `File ${newFileName} in ${path} was created`, color: 'blue' })
  } else {
    LOG({ text: `File ${newFileName} in ${path} was NOT created`, color: 'red' })
  }
  const fileDeleted = await FS.DELETE_FILE({ path: path + newFileName })
  if ( fileDeleted.success ) {
    LOG({ text: `File ${newFileName} in ${path} was deleted`, color: 'blue' })
  } else {
    LOG({ text: `File ${newFileName} in ${path} was NOT deleted`, color: 'red' })
  }
}

init()
