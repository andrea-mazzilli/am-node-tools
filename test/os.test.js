const { LOG, OS } = require('../index')

const init = () => {
  LOG({ text: 'TEST OS', color: 'green', clear: true })
  const osPlatform = OS()
  LOG({ text: `Your OS is ${osPlatform}`, color: 'blue' })
}

init()
