const { LOG, WAIT } = require('../index')

const init = async () => {
  LOG({ text: 'TEST WAIT', color: 'green', clear: true })

  LOG({ text: 'wait 2000 ms', color: 'yellow' })
  await WAIT({ ms: 2000 })
  LOG({ text: 'wait 2000 ms done!', color: 'green' })

  LOG({ text: 'wait 1000 ms', color: 'yellow' })
  await WAIT({ ms: 1000 })
  LOG({ text: 'wait 1000 ms done!', color: 'green' })

  LOG({ text: 'wait 3000 ms', color: 'yellow' })
  await WAIT({ ms: 3000 })
  LOG({ text: 'wait 3000 ms done!', color: 'green' })
}

init()