const { LOG, DATE } = require('../index')

const init = async () => {
  LOG({ text: 'TEST DATE', color: 'green', clear: true })
  LOG({ text: 'TIMESTAMP' })
  LOG({ text: DATE.TIMESTAMP() , color: 'green' })
}

init()