const {FS, EXEC, WAIT, LOG} = require('./index')
const {spawn} = require('child_process');

const SPAWN = async ({command, args}) => {
  return new Promise(resolve => {
    let result = ''
    const cmd = spawn( command, args );
    cmd.stdout.on('data', (data) => {
      result += `${data}`
    });
    cmd.stderr.on('data', (data) => {
      resolve(false)
    });
    cmd.on('close', (code) => {
      console.log(result)
      resolve(true)
    });
  })

}


const init = async () => {
  let testFiles = await FS.READ_FOLDER({path: './test'})
  testFiles = testFiles.data
  for (let x in testFiles) {
    LOG({ text: testFiles[ x ] })
    const cmd = await SPAWN({ command: 'node', args: [ './test/'+testFiles[ x ] ] })
    await WAIT({ ms: 10 })
    if ( !cmd ) {
      LOG({ text: testFiles[ x ], color: 'red' })
      break
    }
  }
  //EXEC({command: 'node test/log.test.js'})
} 

init()
