const { LOG, ASK } = require('../index')

const init = async () => {
  LOG({ text: 'TEST ASK', color: 'green', clear: true })
  const name = await ASK({ 
    text: 'What is your name?', 
    type: 'text',
    error: 'Please check your input',
    expected: input => input !== ''
  })
  LOG({ text: `Your name is ${name}`, color: 'blue' })
  const age = await ASK({ 
    text: 'How old are you? (0-200)', 
    type: 'number',
    error: 'Please check your input',
    expected: input => input <= 200 && input >= 0 && input !== ''
  })
  LOG({ text: `Your age is ${age}`, color: 'blue' })
  const email = await ASK({ 
    text: 'Enter your email', 
    type: 'text',
    error: 'Please check your input',
    expected: input => input !== '' && input.indexOf('@') > -1 && input.indexOf('.') > -1
  })
  LOG({ text: `Your email is ${email}`, color: 'blue' })
}

init()
