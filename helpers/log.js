const colors = require('colors')

const log = ({ text, color, clear }) => {
  if ( clear ) {
    console.log('\n')
  }
  const newColor = color ? color : 'white'
  console.log( text.toString()[ newColor ] )
}

module.exports = log