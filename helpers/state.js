let state = {}

const get = key => {
  if ( key ) {
    return state[ key ] ? JSON.parse( JSON.stringify( state[ key ] ) ) : undefined
  } else {
    return JSON.parse( JSON.stringify( state ) )
  }
}

const set = ({ key, value }) => {
  if ( key ) {
    state[ key ] = value
  }
  return state
}

const erase = () => {
  state = {}
  return state
}

const init = payload => { state = payload; return state }

module.exports = {
  INIT: init,
  GET: get,
  SET: set,
  ERASE: erase
}