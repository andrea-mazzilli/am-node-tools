const prompts = require('prompts')

const ask = async ({ text, type, initial, expected, choices, error }) => {
  const response = await prompts({
    type,
    name: 'value',
    message: text,
    error,
    initial: initial ? initial : '',
    validate: expected,
    choices
  });
  return response.value;
}

module.exports = ask