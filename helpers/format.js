const jsonBeautify = require('json-beautify')
const { MS_IN_DAY, MS_IN_HOUR, MS_IN_MINUTE, MS_IN_SECOND } = require('./date')

const json = payload => jsonBeautify( payload, null, 2, 0)
const friendlyDate = timestamp => {
  let result = '?'
  if ( timestamp ) {
    const now = new Date().getTime()
    const timeDiff = now - parseInt( timestamp, 0 )
    if ( timeDiff > MS_IN_DAY ) {
      const days = parseInt( timeDiff, 0 ) / MS_IN_DAY
      result = `${days} days ago`
    }
    if ( timeDiff > MS_IN_HOUR && timeDiff < MS_IN_DAY ) {
      const hours = parseInt( timeDiff, 0 ) / MS_IN_HOUR
      result = `${hours} hours ago`
    }
    if ( timeDiff > MS_IN_MINUTE && timeDiff < MS_IN_HOUR ) {
      const minutes = parseInt( timeDiff, 0 ) / MS_IN_MINUTE
      result = `${minutes} minutes ago`
    }
    if ( timeDiff < MS_IN_MINUTE ) {
      result = `less than a minute ago`
    }
  }
  return result
}

module.exports = {
  JSON: json,
  FRIENDLY_DATE: friendlyDate
}