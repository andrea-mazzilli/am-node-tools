const os = require('os')

const OS = () => {
  return os.platform()
}

module.exports = OS