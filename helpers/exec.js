const { exec } = require('child_process');

const EXEC = async ({ command }) => {
  return new Promise((resolve, reject) => {
    exec( command, (err, stdout, stderr) => {
      if (err) {
        resolve(false)
      } else if ( stderr ) {
        resolve( stderr )
      } else {
        resolve( stdout )
      }
    })
  })
}

module.exports = EXEC