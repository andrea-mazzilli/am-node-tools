const Jimp = require('jimp')


const codeToChar = code => String.fromCharCode(code)

const stringToPixels = string => {
  const temp = []
  for ( let x = 0; x < string.length; ++x ) {
    const result = string.charCodeAt(x)
    temp.push( result )
  }
  return temp
}

const pngToJson = async ({ path }) => {
  return new Promise( resolve => {
    let result = ''
    Jimp.read( path )
    .then(data => {
      const width = data.getWidth()
      const height = data.getHeight()
      for ( let y = 0; y < height; ++y ) {
        for ( let x = 0; x < width; ++x ) {
          const pixel = data.getPixelColor( x, y )
          if ( pixel ) { result += codeToChar( pixel ) }
        }
      }
      resolve( result )
    })
    .catch(err => {
      resolve( false )
    });
  })
}

const jsonToPng = async ({ json, path }) => {
  return new Promise( resolve => {
    const sourceJSON = JSON.stringify( json )
    const pixels = stringToPixels( sourceJSON )
    const size = Math.ceil( Math.sqrt( sourceJSON.length ) )
    let image = new Jimp(size, size, function (err, image) {
      if (err) {
        resolve(false)
      } else {
        let y = 0
        let x = 0
        pixels.forEach( (color, i) => {
          if ( x === size ) {
            x = 0
            y++
          }
          image.setPixelColor(color, x, y)
          x++
        });
      
        image.write( path, (err) => {
          if (err) {
            resolve(false)
          } else {
            resolve(true)
          }
        });
      }
    });
  })
}

module.exports = {
  PNG_TO_JSON: pngToJson,
  JSON_TO_PNG: jsonToPng
}