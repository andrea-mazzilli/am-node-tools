const getTimestamp = () => new Date().getTime()
const msInSecond = 1000
const msInMinute = msInSecond * 60
const msInHour = msInMinute * 60
const msInDay = msInHour * 24

module.exports = {
  MS_IN_SECOND: msInSecond,
  MS_IN_MINUTE: msInMinute,
  MS_IN_HOUR: msInHour,
  MS_IN_DAY: msInDay,
  TIMESTAMP: getTimestamp
}