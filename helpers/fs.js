const fs = require('fs')

const IS_WRITABLE = async ({ path }) => {
  return new Promise((resolve, reject) => {
    fs.access(path, fs.constants.R_OK | fs.constants.W_OK, (err) => {
      if (err) {
        resolve({ success: false, err })
      } else {
        resolve({ success: true })
      }
    })
  })
}

const CREATE_FOLDER = async ({ path, name }) => {
  return new Promise((resolve, reject) => {
    fs.mkdir( `${path}/${name}`.replace('//','/'), () => {
      resolve({ success: true })
    })
  })
}

const CREATE_FILE = async ({ path, name, data, encoding }) => {
  return new Promise((resolve, reject) => {
    fs.writeFile( `${path}/${name}`.replace('//','/'), data, { encoding: encoding ? encoding : 'utf-8' }, ( err ) => {
      if (err) {
        resolve({ success: false, err })
      } else {
        resolve({ success: true })
      }
    })
  })
}

const DELETE_FILE = async ({ path }) => {
  return new Promise((resolve, reject) => {
    fs.unlink( path, (err) => {
      if (err) {
        resolve({ success: false, err })
      } else {
        resolve({ success: true })
      }
    })
  })
}

const DELETE_FOLDER = async ({ path }) => {
  return new Promise((resolve, reject) => {
    fs.rmdir( path, { recursive: true }, () => {
      resolve({ success: true })
    })
  })
}

const READ_FOLDER = async ({ path, exclude }) => {
  return new Promise((resolve,reject) => {
    fs.readdir( path, { encoding: 'utf-8' }, (err, data) => {
      if (err) {
        resolve({ success: false, err })
      } else {
        resolve({ success: true, data })
      }
    })
  })
}

const READ_FILE = async ({ path }) => {
  return new Promise((resolve,reject) => {
    fs.readFile( path, { encoding: 'utf-8' }, (err, data) => {
      if (err) {
        resolve({ success: false, err })
      } else {
        resolve({ success: true, data })
      }
    })
  })
}

module.exports = {
  IS_WRITABLE,
  CREATE_FOLDER,
  CREATE_FILE,
  READ_FOLDER,
  READ_FILE,
  DELETE_FILE,
  DELETE_FOLDER
}